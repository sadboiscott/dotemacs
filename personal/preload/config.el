;; Preload configuration (custom)

;; Apply different theme
    (let ((basedir "~/.emacs.d/themes/"))
      (dolist (f (directory-files basedir))
        (if (and (not (or (equal f ".") (equal f "..")))
                 (file-directory-p (concat basedir f)))
            (add-to-list 'custom-theme-load-path (concat basedir f)))))
(setq prelude-theme 'sanityinc-tomorrow-night)
